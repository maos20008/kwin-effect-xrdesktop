xrdesktop effect for KWin
=========================

Mirror desktop windows into VR

## Activating the Effect

Find and activate the effect in the KDE System Settings -> Desktop Behavior -> Desktop Effects section or open the Desktop Effects KCM directly with:

    kcmshell5 kcm_kwin_effects

The effect is called **xrdesktop**.

The plugin settings offer an option to set a hotkey for toggling xrdesktop.

## Diagnosing issues

xrdesktop outputs some debugging information to stdout. Start kwin from a terminal with

    kwin_11 --replace

to see this output.

## Code of Conduct

Please note that this project is released with a Contributor Code of Conduct.
By participating in this project you agree to abide by its terms.

We follow the standard freedesktop.org code of conduct,
available at <https://www.freedesktop.org/wiki/CodeOfConduct/>,
which is based on the [Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

* First-line project contacts:
  * Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
  * Christoph Haag <christoph.haag@collabora.com>
* freedesktop.org contacts: see most recent list at <https://www.freedesktop.org/wiki/CodeOfConduct/>

