/*
 * KWin XRDesktop Plugin
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef EFFECTS_VRMIRROR_H
#define EFFECTS_VRMIRROR_H

// gobject uses an identifier "signals" that is also a macro from qt
// unless qt's macro is disabled with this define
#define QT_NO_SIGNALS_SLOTS_KEYWORDS

#include <kwineffects.h>

// extern C around C headers prevents C++ linker from mangling symbol names
extern "C" {

/* kwin includes epoxy, so we have to use the epoxy's egl headers too or we get
 * conflicting defines */
#include <epoxy/egl.h>
#define EGL_EGLEXT_PROTOTYPES
#include <EGL/eglext.h>
#include <GL/glext.h>

// Bool macro from xlib headers conflicts with Qt Bool used in Qt headers
#undef Bool

#include <glib.h>
#include <inputsynth.h>
#include <xrd.h>
}

#include <kwinglutils.h>
#include <QDateTime>

// TODO: calculate good value from desktop window, while taking into account
// hidpi scaling. Maybe on wayland we have different scaling per monitor.
#define pixelsPerMeter 450.

/* Sometimes in continually updated windows we don't receive a damage signal
 * before the draw. After damage just submit the next X frames to work around
 * this. */
#define RENDER_NUM_FRAMES_AFTER_DAMAGE 2

class WindowWrapper
{
public:
    WindowWrapper(KWin::EffectWindow *window)
        : kwinWindow(window)
    {
        // printf("New window %p, %p\n", window, kwin_window);
    }
    KWin::EffectWindow *kwinWindow;

    bool keepAboveOrig = false;
    bool keepBelowOrig = false;
    guint keyboardCharSignal = 0;
    guint keyboardCloseSignal = 0;

    /* The offscreen texture kwin renders into to avoid allocating a new
     * offscreen texture every frame */
    QSharedPointer<KWin::GLTexture> offscreenGLTexture = 0;

    /* only used with external memory */
    GulkanTexture *gulkanTexture = 0;

    /* set to RENDER_NUM_FRAMES_AFTER_DAMAGE,
     *decreased when one frame has been rendered */
    int framesToRender = 15;
};

class VRMirror : public KWin::Effect
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.kwin.XR")
    Q_SCRIPTABLE Q_PROPERTY(bool active READ isActive WRITE setActive NOTIFY activeChanged) public
        : VRMirror();
    ~VRMirror();
    static bool supported();
    static bool enabledByDefault();
    bool isActive() const;
    void setActive(bool active);

    void postPaintWindow(KWin::EffectWindow *w);
    void drawWindow(KWin::EffectWindow *w, int mask, QRegion region, KWin::WindowPaintData &data);
    void prePaintScreen(KWin::ScreenPrePaintData &data, int time);
    void postPaintScreen();

    /* sometimes a windowDamaged signal is missing from kwin on continously
     * updated windows rendering one extra frame makes sure there is no stutter.
     * TODO: investigate why kwin does that. */
    const int framesAfterDamage = 2;

    void moveCursor(XrdWindow *xrdWin, graphene_point_t *window_coords);
    void click(XrdWindow *xrdWin, graphene_point_t *window_coords, int btn, bool pressed);

    void upload_window(XrdWindow *xrdWin);
    void setPositionFromDesktop(XrdWindow *xrdWin);

    void activateVRMirror(bool scene);
    void deactivateVRMirror();
    void restoreAboveBelowStatus();

    XrdClient *xrdClient = NULL;
    InputSynth *synth = NULL;
    GulkanTexture *cursorTexture = NULL;

    int cursorHotspotX = 0;
    int cursorHotspotY = 0;

    bool m_vrmirrorRunning = false;

    guint sceneRenderSource = 0;

    guint64 clickSource;
    guint64 moveSource;
    guint64 keyboardSource;
    guint64 quitSource;

    /* counter that gets increased on every mapWindow() and decreases on
     windowClosed()
     not guaranteed to be very accurate, just used for placement of new windows
     on z axis */
    int num_windows = 0;
    bool uploadOnlyDamaged;

    bool onlyCurrentWorkspace = false;

    bool tracePaintTime;
    QTime lastPostPaint;
    QTime lastPrePaint;

    bool sceneModeEnabled;

    QTimer *pollTimer = 0;

private:
public Q_SLOTS:
    void toggleScreenVRMirror();
    void slotWindowClosed(KWin::EffectWindow *w);
    void slotWindowAdded(KWin::EffectWindow *w);
    void slotUpdateCursorTexture();
    XrdWindow *mapWindow(KWin::EffectWindow *win, bool force);
    void glibIterate();
    void perform_switch();
    void damaged(KWin::EffectWindow *w);

Q_SIGNALS:
    Q_SCRIPTABLE void activeChanged(bool);
};

#endif
