#ifndef OLD_EPOXY_COMPAT_H
#define OLD_EPOXY_COMPAT_H

#ifndef PFNGLCREATEMEMORYOBJECTSEXTPROC
typedef void (APIENTRYP PFNGLCREATEMEMORYOBJECTSEXTPROC) (GLsizei n, GLuint *memoryObjects);
#endif

#ifndef PFNGLMEMORYOBJECTPARAMETERIVEXTPROC
typedef void (APIENTRYP PFNGLMEMORYOBJECTPARAMETERIVEXTPROC) (GLuint memoryObject, GLenum pname, const GLint *params);
#endif

#ifndef PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC
typedef void (APIENTRYP PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC) (GLuint memoryObject, GLenum pname, GLint *params);
#endif

#ifndef PFNGLIMPORTMEMORYFDEXTPROC
typedef void (APIENTRYP PFNGLIMPORTMEMORYFDEXTPROC) (GLuint memory, GLuint64 size, GLenum handleType, GLint fd);
#endif

#ifndef PFNGLTEXSTORAGEMEM2DEXTPROC
typedef void (APIENTRYP PFNGLTEXSTORAGEMEM2DEXTPROC) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLuint memory, GLuint64 offset);
#endif

#ifndef PFNGLDELETEMEMORYOBJECTSEXTPROC
typedef void (APIENTRYP PFNGLDELETEMEMORYOBJECTSEXTPROC) (GLsizei n, const GLuint *memoryObjects);
#endif

#ifndef GL_HANDLE_TYPE_OPAQUE_FD_EXT
#define GL_HANDLE_TYPE_OPAQUE_FD_EXT      0x9586
#endif

#ifndef GL_DEDICATED_MEMORY_OBJECT_EXT
#define GL_DEDICATED_MEMORY_OBJECT_EXT    0x9581
#endif

#endif
