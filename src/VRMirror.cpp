/*
 * KWin XRDesktop Plugin
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "kwingltexture.h"
#undef signals

#include "VRMirror.h"

#include <KGlobalAccel>
#include <KLocalizedString>
#include <kwindowsystem.h>
#include <kwinglplatform.h>
#include <kwinglutils.h>
#include <QAction>
#include <QFile>
#include <QMatrix4x4>
#include <QStandardPaths>
#include <QTimer>
#include <QtDebug>

#include <gdk/gdk.h>

#include <gulkan.h>
#include <xrd.h>

// segfault handler
#include <csignal>

// ugh
#undef Unsorted
#undef None
#include <QtDBus>

// typedefs in case epoxy is too old
#include "old_epoxy_compat.h"

void GLAPIENTRY MessageCallback(GLenum source,
                                GLenum type,
                                GLuint id,
                                GLenum severity,
                                GLsizei length,
                                const GLchar *message,
                                const void *userParam)
{
    (void) source;
    (void) id;
    (void) length;
    (void) userParam;
    fprintf(stderr,
            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type,
            severity,
            message);
}

static VRMirror *instance = 0;

static bool isExcludedFromMirroring(KWin::EffectWindow *kwin_window)
{
    /* yes, we do indeed not mirror NULL */
    if (kwin_window == NULL)
        return TRUE;

    return kwin_window->isDesktop() || kwin_window->isDock() || kwin_window->isDNDIcon()
           || kwin_window->width() < 20 || kwin_window->height() < 20;
}

static bool _isDeleted(KWin::EffectWindow *w)
{
#if KWIN_EFFECT_API_VERSION_MINOR >= 227
    bool isDeleted = w->isDeleted();
#else
    const QVariant isDeletedVariant = w->parent()->property("deleted");
    bool isDeleted = isDeletedVariant.toBool();
#endif
    return isDeleted;
}

static XrdWindow *_kwinWindowToXrdWindow(VRMirror *self,
                                         KWin::EffectWindow *w,
                                         bool includeDeleted = false)
{
    if (isExcludedFromMirroring(w))
        return NULL;

    /* this window has been closed, but still receives callbacks for a while */
    if (!includeDeleted && _isDeleted(w))
        return NULL;

    XrdWindow *xrdWin = xrd_client_lookup_window(self->xrdClient, w);
    return xrdWin;
}

/** Menus, ContextMenus, etc. that should be fixed to a parent window and may
 * not be individually moved */
static bool isChildWindow(KWin::EffectWindow *win)
{
    return win->isPopupMenu() || win->isDropdownMenu() || win->isTooltip() || win->isComboBox() ||
        win->isDialog();
}

// functions take char* so we can't use literals
static char keepAboveString[] = "keepAbove";
static char keepBelowString[] = "keepBelow";

static void setWindowProperty(KWin::EffectWindow *win, char *propertyname, bool value)
{
    const QVariant variant = win->parent()->property(propertyname);
    if (variant.isValid()) {
        win->parent()->setProperty(propertyname, value);
    }
}
static void putAbove(KWin::EffectWindow *win)
{
    setWindowProperty(win, keepBelowString, false);
    setWindowProperty(win, keepAboveString, true);
    if (KWin::effects->activeWindow() != win) {
        KWin::effects->activateWindow(win);
    }
}
static void putBelow(KWin::EffectWindow *win)
{
    setWindowProperty(win, keepAboveString, false);
    // setWindowProperty(win, keepBelowString, true);
    // TODO: annoying on --replace or crash
}

/* Resize and move the window so it fully fits on the desktop.
 * This is necessary because the input synthesis uses the X11 mouse pointer and
 * it needs to be able to move to every part of the window.
 */
static void fitWindowOnDesktop(KWin::EffectWindow *win)
{
    win->unminimize();
    QRect windowrect = win->geometry();
    // TODO: some screens can be smaller than others...
    // TODO: panels???
    QRect screenarea = KWin::effects->virtualScreenGeometry();

    // y grows DOWN
    if (windowrect.top() < screenarea.top()) {
        int ydiff = screenarea.top() - windowrect.top();
        windowrect.translate(0, ydiff);
    }
    if (windowrect.bottom() > screenarea.bottom()) {
        int ydiff = windowrect.bottom() - screenarea.bottom();
        windowrect.translate(0, -ydiff);
    }

    if (windowrect.left() < screenarea.left()) {
        int xdiff = screenarea.left() - windowrect.left();
        windowrect.translate(xdiff, 0);
    }
    if (windowrect.right() > screenarea.right()) {
        int xdiff = windowrect.right() - screenarea.right();
        windowrect.translate(-xdiff, 0);
    }

    qDebug() << "Screen:" << screenarea.topLeft() << "," << screenarea.bottomRight()
             << "  Window:" << win->geometry().topLeft() << "," << win->geometry().bottomRight();
    if (windowrect == win->geometry()) {
        qDebug() << "Nothing to move!";
    } else {
        qDebug() << "Moving window" << win->caption() << "from" << win->geometry().topLeft() << ","
                 << win->geometry().bottomRight() << "to" << windowrect.topLeft() << ","
                 << windowrect.bottomRight();
        KWin::effects->moveWindow(win, windowrect.topLeft(), true, 5.0);
    }
}

KWIN_EFFECT_FACTORY_SUPPORTED_ENABLED(VRMirrorFactory,
                                      VRMirror,
                                      "vrmirror.json",
                                      return VRMirror::supported();
                                      , return VRMirror::enabledByDefault();)

/* Coordinate space: 0 == x: left, y == 0: top */
static graphene_point_t _windowToDesktopCoordinates(WindowWrapper *vrWin,
                                                    graphene_point_t *positionOnWindow)
{
    KWin::EffectWindow *kwinWindow = vrWin->kwinWindow;

    QPoint pos = kwinWindow->pos();
    graphene_point_t positionOnDesktop = {pos.x() + positionOnWindow->x,
                                          pos.y() + positionOnWindow->y};
    return positionOnDesktop;
}

static gboolean _iterate_scene_cb(gpointer _self)
{
    VRMirror *self = (VRMirror *) _self;
    xrd_scene_client_render(XRD_SCENE_CLIENT(self->xrdClient));
    return true;
}

static void _click_cb(XrdClient *client, XrdClickEvent *event, VRMirror *self);
static void _move_cursor_cb(XrdClient *client, XrdMoveCursorEvent *event, VRMirror *self);
static void _keyboard_press_cb(XrdClient *client, GdkEventKey *event, VRMirror *self);
static void _systemQuitCallback(XrdClient *xrdClient, OpenVRQuitEvent *event, VRMirror *self);

static void _connectClientSources(VRMirror *self)
{
    self->clickSource = g_signal_connect(self->xrdClient,
                                         "click-event",
                                         (GCallback) _click_cb,
                                         self);
    self->moveSource = g_signal_connect(self->xrdClient,
                                        "move-cursor-event",
                                        (GCallback) _move_cursor_cb,
                                        self);
    self->keyboardSource = g_signal_connect(self->xrdClient,
                                            "keyboard-press-event",
                                            (GCallback) _keyboard_press_cb,
                                            self);
    self->quitSource = g_signal_connect(self->xrdClient,
                                        "request-quit-event",
                                        (GCallback) _systemQuitCallback,
                                        self);
}

static void _disconnectClientSources(VRMirror *self)
{
    if (self->sceneRenderSource != 0)
        g_source_remove(self->sceneRenderSource);
    g_signal_handler_disconnect(self->xrdClient, self->clickSource);
    g_signal_handler_disconnect(self->xrdClient, self->moveSource);
    g_signal_handler_disconnect(self->xrdClient, self->keyboardSource);
    g_signal_handler_disconnect(self->xrdClient, self->quitSource);
    self->clickSource = 0;
    self->moveSource = 0;
    self->keyboardSource = 0;
    self->quitSource = 0;
    self->sceneRenderSource = 0;
}

static void _destroyTextures(VRMirror *self)
{
    GSList *windows = xrd_client_get_windows(self->xrdClient);
    for (GSList *l = windows; l != NULL; l = l->next) {
        XrdWindow *window = (XrdWindow *) l->data;
        WindowWrapper *native = nullptr;
        g_object_get(window, "native", &native, NULL);
        g_clear_object(&native->gulkanTexture);

        // external memory texture, has to be recreated
        native->offscreenGLTexture = nullptr;
    }
    g_clear_object(&self->cursorTexture);
}

void VRMirror::perform_switch()
{
    _disconnectClientSources(this);

    // gulkan textures become invalid in a new client instance
    _destroyTextures(this);

    xrdClient = xrd_client_switch_mode(xrdClient);
    _connectClientSources(this);

    /* after the switch, render the next 15 frames to make sure all windows have
     * textures */
    GSList *windows = xrd_client_get_windows(xrdClient);
    for (GSList *l = windows; l; l = l->next) {
        XrdWindow *xrdWin = XRD_WINDOW(l->data);

        WindowWrapper *vrWin;
        g_object_get(xrdWin, "native", &vrWin, NULL);
        vrWin->framesToRender = 15;
    }
}

VRMirror::VRMirror()
    : m_vrmirrorRunning(false)
{
    instance = this;

    qDebug() << "Starting xrdesktop plugin init.";

    auto *toggleAction = new QAction(this);
    toggleAction->setObjectName(QStringLiteral("Toggle"));
    toggleAction->setText(i18n("Toggle Mirroring Windows to VR with xrdesktop"));
    KGlobalAccel::self()->setDefaultShortcut(toggleAction, {});
    KGlobalAccel::self()->setShortcut(toggleAction, {});
    KWin::effects->registerGlobalShortcut({}, toggleAction);

    connect(toggleAction, &QAction::triggered, this, &VRMirror::toggleScreenVRMirror);
    connect(KWin::effects, &KWin::EffectsHandler::windowClosed, this, &VRMirror::slotWindowClosed);
    connect(KWin::effects, &KWin::EffectsHandler::windowAdded, this, &VRMirror::slotWindowAdded);
    connect(KWin::effects,
            &KWin::EffectsHandler::cursorShapeChanged,
            this,
            &VRMirror::slotUpdateCursorTexture);

    if (!QDBusConnection::sessionBus().registerObject(QStringLiteral("/XR"),
                                                      this,
                                                      QDBusConnection::ExportScriptableProperties
                                                          | QDBusConnection::ExportScriptableSignals
                                                          | QDBusConnection::ExportScriptableSlots)) {
        qDebug() << "Failed to register DBus object";
    }

    connect(KWin::effects,
            SIGNAL(windowDamaged(KWin::EffectWindow *, QRect)),
            SLOT(damaged(KWin::EffectWindow *)));

    QString sceneModeEnabledStr = QProcessEnvironment::systemEnvironment()
                                      .value("SCENE_MODE_ENABLED", "");
    sceneModeEnabled = sceneModeEnabledStr != "";

    QString tracePaintTimeStr = QProcessEnvironment::systemEnvironment().value("TRACE_PAINT_TIME",
                                                                               "");
    tracePaintTime = tracePaintTimeStr != "";

    QString uploadOnlyDamagedStr = QProcessEnvironment::systemEnvironment()
                                       .value("UPLOAD_ONLY_DAMAGED", "TRUE");
    uploadOnlyDamaged = uploadOnlyDamagedStr != "";

    QString onlyCurrentWorkspaceStr = QProcessEnvironment::systemEnvironment()
                                       .value("MIRROR_ONLY_CURRENT_WORKSPACE", "");
    onlyCurrentWorkspace = onlyCurrentWorkspaceStr != "";

    QString glDebugEnabled = QProcessEnvironment::systemEnvironment().value("GL_DEBUG_ENABLED", "");
    if (glDebugEnabled != "") {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(MessageCallback, 0);
    }

    qDebug() << "Initializing xrdesktop plugin  successful.";
}

void VRMirror::damaged(KWin::EffectWindow *w)
{
    if (!m_vrmirrorRunning)
        return;

    XrdWindow *xrdWin = _kwinWindowToXrdWindow(this, w);
    if (!xrdWin)
        return;

    WindowWrapper *native = nullptr;
    g_object_get(xrdWin, "native", &native, NULL);
    native->framesToRender = std::max(RENDER_NUM_FRAMES_AFTER_DAMAGE, native->framesToRender);
}

static bool isDamaged(VRMirror *self, KWin::EffectWindow *kwin_window)
{
    XrdWindow *xrdWin = _kwinWindowToXrdWindow(self, kwin_window);
    if (!xrdWin)
        return false;

    WindowWrapper *native = nullptr;
    g_object_get(xrdWin, "native", &native, NULL);
    return native->framesToRender > 0;
}
static void damageProcessed(VRMirror *self, KWin::EffectWindow *kwin_window)
{
    XrdWindow *xrdWin = _kwinWindowToXrdWindow(self, kwin_window);
    if (!xrdWin)
        return;

    WindowWrapper *native = nullptr;
    g_object_get(xrdWin, "native", &native, NULL);
    native->framesToRender -= 1;
}

void VRMirror::restoreAboveBelowStatus()
{
    GSList *windows = xrd_client_get_windows(xrdClient);
    for (GSList *l = windows; l; l = l->next) {
        XrdWindow *xrdWin = XRD_WINDOW(l->data);

        WindowWrapper *vrWin;
        g_object_get(xrdWin, "native", &vrWin, NULL);

        setWindowProperty(vrWin->kwinWindow, keepAboveString, vrWin->keepAboveOrig);
        setWindowProperty(vrWin->kwinWindow, keepBelowString, vrWin->keepBelowOrig);
    }
}

static void _ensure_on_workspace(KWin::EffectWindow *kwin_window)
{
    uint32_t currentDesktop = KWin::effects->currentDesktop();

    // property lookup still works, but the new function is faster!
#if KWIN_EFFECT_API_VERSION_MINOR >= 227
    QVector<uint> windowDesktops = kwin_window->desktops();
#else
    const QVariant variant = kwin_window->parent()->property("x11DesktopIds");
    QVector<uint> windowDesktops = variant.value<QVector<uint>>();
#endif

    // If the list is empty it means the window is on all desktops
    if (windowDesktops.size() == 0)
        return;

    // On X11 this list will always have a length of 1, on Wayland can be any
    // subset.
    // TODO: wayland
    if (currentDesktop != windowDesktops[0]) {
        KWin::effects->setCurrentDesktop(windowDesktops[0]);
    }
}

static void _click_cb(XrdClient *client, XrdClickEvent *event, VRMirror *self)
{
    (void) client;
    (void) self;
    // g_print ("click: %f, %f\n", event->position->x, event->position->y);

    XrdWindow *xrdWin = event->window;
    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);

    if (!vrWin || !vrWin->kwinWindow || _isDeleted(vrWin->kwinWindow))
        return;

    _ensure_on_workspace(vrWin->kwinWindow);

    if (KWin::effects->activeWindow() != vrWin->kwinWindow) {
        KWin::effects->activateWindow(vrWin->kwinWindow);
    }

    graphene_point_t xy = _windowToDesktopCoordinates(vrWin, event->position);

    qDebug() << (event->state ? "Pressing " : "Releasing ") << " button " << event->button << "at"
             << xy.x << ", " << xy.y;

    input_synth_click(self->synth, xy.x, xy.y, event->button, event->state);
}

static guint64 last;
static void _move_cursor_cb(XrdClient *client, XrdMoveCursorEvent *event, VRMirror *self)
{
    (void) client;
    (void) self;
    // g_print ("click: %f, %f\n", event->position->x, event->position->y);

    if (event->ignore) {
        qDebug() << "Ignored event";
        return;
    }

    XrdWindow *xrdWin = event->window;
    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);

    if (!vrWin || !vrWin->kwinWindow || _isDeleted(vrWin->kwinWindow))
        return;

    _ensure_on_workspace(vrWin->kwinWindow);

    if (KWin::effects->activeWindow() != vrWin->kwinWindow) {
        KWin::effects->activateWindow(vrWin->kwinWindow);
    }

    graphene_point_t xy = _windowToDesktopCoordinates(vrWin, event->position);

    guint64 now = g_get_monotonic_time();
    // qDebug() << "Synth input after: " << (now - last) / 1000.;
    last = now;
    input_synth_move_cursor(self->synth, xy.x, xy.y);
}

static void _keyboard_press_cb(XrdClient *client, GdkEventKey *event, VRMirror *self)
{
    if (!xrd_client_get_keyboard_window(client)) {
        qDebug() << "ERROR: No keyboard window!";
        return;
    }

    XrdWindow *keyboardXrdWin = xrd_client_get_keyboard_window(client);
    WindowWrapper *keyboardVrWin;
    g_object_get(keyboardXrdWin, "native", &keyboardVrWin, NULL);

    if (!keyboardVrWin || !keyboardVrWin->kwinWindow || _isDeleted(keyboardVrWin->kwinWindow))
        return;

    _ensure_on_workspace(keyboardVrWin->kwinWindow);

    if (KWin::effects->activeWindow() != keyboardVrWin->kwinWindow) {
        KWin::effects->activateWindow(keyboardVrWin->kwinWindow);
    }

    qDebug() << "Keyboard Input:" << event->string;
    for (int i = 0; i < event->length; i++) {
        input_synth_character(self->synth, event->string[i]);
    }
}

void _updateCursorImage(VRMirror *mirror, KWin::PlatformCursorImage *cursorImage)
{
    QPoint cursorHotspot = cursorImage->hotSpot();

    uint32_t newWidth = cursorImage->image().width();
    uint32_t newHeight = cursorImage->image().height();

    /* rare case that crashes if we don't return early */
    if (newWidth == 0 || newHeight == 0)
        return;

    QImage formatted = cursorImage->image().convertToFormat(QImage::Format_RGBA8888);
    uchar *rgba = formatted.bits();

    VkImageLayout layout = xrd_client_get_upload_layout(mirror->xrdClient);

    GdkPixbuf *pixbuf = gdk_pixbuf_new_from_data(rgba,
                                                 GDK_COLORSPACE_RGB,
                                                 TRUE,
                                                 8,
                                                 newWidth,
                                                 newHeight,
                                                 4 * newWidth,
                                                 NULL,
                                                 NULL);
    GulkanClient *uploader = xrd_client_get_uploader(mirror->xrdClient);

    if (mirror->cursorTexture == NULL || gulkan_texture_get_width(mirror->cursorTexture) != newWidth
        || gulkan_texture_get_height(mirror->cursorTexture) != newHeight) {
        if (mirror->cursorTexture) {
            g_object_unref(mirror->cursorTexture);
        }

        mirror->cursorTexture = gulkan_client_texture_new_from_pixbuf(uploader,
                                                                      pixbuf,
                                                                      VK_FORMAT_R8G8B8A8_UNORM,
                                                                      layout,
                                                                      false);
    } else {
        gulkan_client_upload_pixbuf(uploader, mirror->cursorTexture, pixbuf, layout);
    }
    xrd_client_submit_cursor_texture(mirror->xrdClient,
                                     uploader,
                                     mirror->cursorTexture,
                                     cursorHotspot.x(),
                                     cursorHotspot.y());

    // qDebug() << "Uploading" << width << "x" << height;
}

void VRMirror::slotUpdateCursorTexture()
{
    if (!m_vrmirrorRunning)
        return;

    KWin::PlatformCursorImage cursorImage = KWin::effects->cursorImage();
    _updateCursorImage(this, &cursorImage);
}

static void _systemQuitCallback(XrdClient *xrdClient, OpenVRQuitEvent *event, VRMirror *self)
{
    (void) xrdClient;
    g_print("Handling VR quit event\n");

    // don't toggle VR from this callback because it will return to xrdClient
    switch (event->reason) {
    case VR_QUIT_SHUTDOWN: {
        g_print("Quit event: Shutdown\n");
        QTimer::singleShot(0, self, SLOT(toggleScreenVRMirror()));
    } break;
    case VR_QUIT_PROCESS_QUIT: {
        g_print("Quit event: Process quit\n");
        if (!self->sceneModeEnabled)
            return;
        QTimer::singleShot(0, self, SLOT(perform_switch()));
    } break;
    case VR_QUIT_APPLICATION_TRANSITION: {
        g_print("Quit event: Application transition\n");
        if (!self->sceneModeEnabled)
            return;
        QTimer::singleShot(0, self, SLOT(perform_switch()));
    } break;
    }
}

VRMirror::~VRMirror()
{
    // qDebug() << "VRMirror plugin destroyed";
    if (m_vrmirrorRunning)
        restoreAboveBelowStatus();
    QDBusConnection::sessionBus().unregisterObject(QStringLiteral("/XR"));
}

bool VRMirror::supported()
{
    return KWin::effects->isOpenGLCompositing();
}

bool VRMirror::enabledByDefault()
{
    return supported();
}

void VRMirror::glibIterate()
{
    // xrdesktop creates a glib main context for us, so we use that default one
    // with the NULL parameter
    while (g_main_context_pending(NULL)) {
        g_main_context_iteration(NULL, FALSE);
    }
}

void VRMirror::prePaintScreen(KWin::ScreenPrePaintData &data, int time)
{
    KWin::effects->prePaintScreen(data, time);
    lastPrePaint = QTime::currentTime();
}

void VRMirror::drawWindow(KWin::EffectWindow *w,
                          int mask,
                          QRegion region,
                          KWin::WindowPaintData &data)
{
    if (m_vrmirrorRunning && _kwinWindowToXrdWindow(this, w)) {
        // qDebug() << "draw window " << w->caption();
        w->addRepaintFull();
    }

    KWin::effects->drawWindow(w, mask, region, data);
}

void VRMirror::setPositionFromDesktop(XrdWindow *xrdWin)
{
    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);
    int desktopHeight = KWin::effects->virtualScreenGeometry().height();
    int desktopWidth = KWin::effects->virtualScreenGeometry().width();

    QPoint p = vrWin->kwinWindow->geometry().center();
    p.setY(desktopHeight * 0.75 - p.y());
    p.setX(p.x() - desktopWidth / 2.);

    graphene_point3d_t point = {static_cast<float>(p.x() / pixelsPerMeter),
                                static_cast<float>(p.y() / pixelsPerMeter),
                                -4.0f + ((float) num_windows) / 6.f};

    graphene_matrix_t transform;
    graphene_matrix_init_translate(&transform, &point);

    xrd_window_set_transformation(xrdWin, &transform);

    xrd_window_set_reset_transformation(xrdWin, &transform);
}

// TODO: clean up function pointer loading. Move to xrdesktop?
PFNGLCREATEMEMORYOBJECTSEXTPROC _glCreateMemoryObjectsEXT = 0;
PFNGLMEMORYOBJECTPARAMETERIVEXTPROC _glMemoryObjectParameterivEXT = 0;
// PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC _glGetMemoryObjectParameterivEXT = 0;
PFNGLIMPORTMEMORYFDEXTPROC _glImportMemoryFdEXT = 0;
PFNGLTEXSTORAGEMEM2DEXTPROC _glTexStorageMem2DEXT = 0;
PFNGLDELETEMEMORYOBJECTSEXTPROC _glDeleteMemoryObjectsEXT = 0;
extern "C" {
/* forward declared to avoid include mess */
extern void (*glXGetProcAddress(const GLubyte *procname))(void);
// pulled in through epoxy egl header: extern void (*eglGetProcAddress(const
// char *procname))(void);
}
void loadGLExtPtrs()
{
    if (KWin::GLPlatform::instance()->platformInterface() == KWin::GlxPlatformInterface) {
        _glCreateMemoryObjectsEXT = (PFNGLCREATEMEMORYOBJECTSEXTPROC) glXGetProcAddress(
            (GLubyte *) "glCreateMemoryObjectsEXT");
        _glMemoryObjectParameterivEXT = (PFNGLMEMORYOBJECTPARAMETERIVEXTPROC) glXGetProcAddress(
            (GLubyte *) "glMemoryObjectParameterivEXT");
        //_glGetMemoryObjectParameterivEXT =
        //(PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC
        //)glXGetProcAddress((GLubyte*)"glGetMemoryObjectParameterivEXT");
        _glImportMemoryFdEXT = (PFNGLIMPORTMEMORYFDEXTPROC) glXGetProcAddress(
            (GLubyte *) "glImportMemoryFdEXT");
        _glTexStorageMem2DEXT = (PFNGLTEXSTORAGEMEM2DEXTPROC) glXGetProcAddress(
            (GLubyte *) "glTexStorageMem2DEXT");
        _glDeleteMemoryObjectsEXT = (PFNGLDELETEMEMORYOBJECTSEXTPROC) glXGetProcAddress(
            (GLubyte *) "glDeleteMemoryObjectsEXT");
    } else if (KWin::GLPlatform::instance()->platformInterface() == KWin::EglPlatformInterface) {
        _glCreateMemoryObjectsEXT = (PFNGLCREATEMEMORYOBJECTSEXTPROC) eglGetProcAddress(
            "glCreateMemoryObjectsEXT");
        _glMemoryObjectParameterivEXT = (PFNGLMEMORYOBJECTPARAMETERIVEXTPROC) eglGetProcAddress(
            "glMemoryObjectParameterivEXT");
        //_glGetMemoryObjectParameterivEXT =
        //(PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC
        //)eglGetProcAddress("glGetMemoryObjectParameterivEXT");
        _glImportMemoryFdEXT = (PFNGLIMPORTMEMORYFDEXTPROC) eglGetProcAddress(
            "glImportMemoryFdEXT");
        _glTexStorageMem2DEXT = (PFNGLTEXSTORAGEMEM2DEXTPROC) eglGetProcAddress(
            "glTexStorageMem2DEXT");
        _glDeleteMemoryObjectsEXT = (PFNGLDELETEMEMORYOBJECTSEXTPROC) eglGetProcAddress(
            "glDeleteMemoryObjectsEXT");
    } else {
        qDebug() << "ERROR: Can only load function pointers on GLX or EGL!";
    }
    if (_glCreateMemoryObjectsEXT == 0) {
        qDebug() << "ERROR: Loading function pointers failed!";
    }
}

void VRMirror::upload_window(XrdWindow *xrdWin)
{
    if (xrdWin == NULL) {
        qDebug() << "Window null";
        return;
    }

    if (!xrd_window_is_visible(xrdWin)) {
        return;
    }

    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);

    // already closed
    if (!vrWin->kwinWindow) {
        return;
    }

    if (uploadOnlyDamaged && !isDamaged(this, vrWin->kwinWindow)) {
        // qDebug() << "Skipping upload of undamaged" <<
        // vrWin->kwinWindow->caption();
        return;
    }
    // qDebug() << "Damaged (" << windowDamaged[vrWin->kwinWindow] << " ):" <<
    // vrWin->kwinWindow->caption();

    damageProcessed(this, vrWin->kwinWindow);

    // this texture copy method is taken from the screenshot effect
    // https://github.com/KDE/kwin/blob/2043161889f57f4a34eaf7852f5f9f758fb00790/effects/screenshot/screenshot.cpp#L99
    KWin::WindowPaintData d(vrWin->kwinWindow);
    double left = 0;
    double top = 0;
    double right = vrWin->kwinWindow->width();
    double bottom = vrWin->kwinWindow->height();

    const int width = right - left;
    const int height = bottom - top;

    if (!vrWin->offscreenGLTexture || width != vrWin->offscreenGLTexture->width()
        || height != vrWin->offscreenGLTexture->height()) {
        /* When using external memory we do not want to initialize the texture
         because we want to import vk memory into it */
        bool initialize = false;

        qDebug() << "Reallocationg GL texture for" << vrWin->kwinWindow->caption() << "---"
                 << (vrWin->offscreenGLTexture ? vrWin->offscreenGLTexture->width() : 0) << "x"
                 << (vrWin->offscreenGLTexture ? vrWin->offscreenGLTexture->height() : 0) << "->"
                 << width << "x" << height << "GL Texture ID:"
                 << (vrWin->offscreenGLTexture ? vrWin->offscreenGLTexture->texture() : 0);

        vrWin->offscreenGLTexture = QSharedPointer<KWin::GLTexture>::create(GL_RGBA8,
                                                                            width,
                                                                            height,
                                                                            1,
                                                                            initialize);

        if (vrWin->gulkanTexture) {
            g_object_unref(vrWin->gulkanTexture);
        }
        GulkanClient *client = xrd_client_get_uploader(xrdClient);
        gsize size;
        int fd;
        GulkanDevice *device = gulkan_client_get_device(client);
        vrWin->gulkanTexture = gulkan_texture_new_export_fd(device,
                                                            width,
                                                            height,
                                                            VK_FORMAT_R8G8B8A8_UNORM,
                                                            &size,
                                                            &fd);
        if (_glCreateMemoryObjectsEXT == 0) {
            loadGLExtPtrs();
        }
        GLuint glExtMemObject = 0;
        _glCreateMemoryObjectsEXT(1, &glExtMemObject);

        GLint glDedicatedMem = GL_TRUE;
        _glMemoryObjectParameterivEXT(glExtMemObject,
                                      GL_DEDICATED_MEMORY_OBJECT_EXT,
                                      &glDedicatedMem);
        //_glGetMemoryObjectParameterivEXT (w->gl_mem_object,
        // GL_DEDICATED_MEMORY_OBJECT_EXT, &glDedicatedMem);
        _glImportMemoryFdEXT(glExtMemObject, size, GL_HANDLE_TYPE_OPAQUE_FD_EXT, fd);

        vrWin->offscreenGLTexture->bind();
        _glTexStorageMem2DEXT(GL_TEXTURE_2D, 1, GL_RGBA8, width, height, glExtMemObject, 0);

        /* apparently we don't need to keep this around */
        _glDeleteMemoryObjectsEXT(1, &glExtMemObject);

        xrd_window_set_flip_y(xrdWin, true);
        qDebug() << "Imported vk memory size" << size << " from fd" << fd
                 << "into OpenGL memory object" << glExtMemObject;

        gulkan_client_transfer_layout(client,
                                      vrWin->gulkanTexture,
                                      VK_IMAGE_LAYOUT_UNDEFINED,
                                      VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    }

    QScopedPointer<KWin::GLRenderTarget> target;
    target.reset(new KWin::GLRenderTarget(*vrWin->offscreenGLTexture));

    if (target->valid()) {
        KWin::GLRenderTarget::pushRenderTarget(target.data());

        d.setXTranslation(-vrWin->kwinWindow->x() - left);
        d.setYTranslation(-vrWin->kwinWindow->y() - top);

        int mask = VRMirror::PAINT_WINDOW_TRANSFORMED | VRMirror::PAINT_WINDOW_TRANSLUCENT;

        QMatrix4x4 projection;
        projection.ortho(
            QRect(0, 0, vrWin->offscreenGLTexture->width(), vrWin->offscreenGLTexture->height()));
        d.setProjectionMatrix(projection);

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);

        xrd_window_submit_texture(XRD_WINDOW(xrdWin),
                                  xrd_client_get_uploader(xrdClient),
                                  vrWin->gulkanTexture);

        KWin::GLRenderTarget::popRenderTarget();
    } else {
        qDebug() << "Error: Render target not valid";
    }
}

void VRMirror::postPaintWindow(KWin::EffectWindow *w)
{
    if (m_vrmirrorRunning) {
        XrdWindow *xrdWin = _kwinWindowToXrdWindow(this, w);
        if (!xrdWin) {
            return;
        }

        upload_window(xrdWin);
    }

    KWin::effects->postPaintWindow(w);
}

void VRMirror::postPaintScreen()
{
    // TODO: iterating the scene renderer after kwin rendered a frame is a HACK.
    // It is necessary because iterating the scene renderer blocks the main
    // thread, synchronizing to the HMD refresh rate. It will still look bad,
    // but at least it will result in a sequence alternating between kwin and
    // scene renderer pushing out a frame:
    // 1. kwin renders a frame for the desktop.
    // 2. the scene renderer renders a frame for the HMD.
    // 3. the main loop is blocked until the next hmd frame.
    if (XRD_IS_SCENE_CLIENT(xrdClient)) {
        _iterate_scene_cb(this);
    }

    KWin::effects->postPaintScreen();

    QTime now = QTime::currentTime();
    if (tracePaintTime) {
        qDebug() << "Paint screen took" << lastPrePaint.msecsTo(now) << "ms; "
                 << "Frametime" << lastPostPaint.msecsTo(now) << "ms";
    }

    lastPostPaint = now;
}

void VRMirror::slotWindowClosed(KWin::EffectWindow *w)
{
    if (!m_vrmirrorRunning) {
        return;
    }

    qDebug() << "Window closed: " << w->caption();
    XrdWindow *xrdWin = _kwinWindowToXrdWindow(this, w, true);
    if (!xrdWin) {
        qDebug() << "Closed window without xrdwin!";
        return;
    }

    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);
    if (!vrWin) {
        qDebug() << "Closed window without native!";
        return;
    }
    delete vrWin;
    g_object_set(xrdWin, "native", NULL, NULL);

    xrd_client_remove_window(instance->xrdClient, xrdWin);

    xrd_window_close(xrdWin);
    g_object_unref(xrdWin);

    num_windows--;
}

void VRMirror::slotWindowAdded(KWin::EffectWindow *w)
{
    if (!m_vrmirrorRunning) {
        return;
    }

    mapWindow(w, false);
}

XrdWindow *VRMirror::mapWindow(KWin::EffectWindow *win, bool force)
{
    if (win->width() > 5 && win->height() > 5) {
    } else {
        qDebug() << "Window too small, not adding:" << win->caption() << win->width() << "x"
                 << win->height();
        return NULL;
    }

    if (force) {
    } else {
        if (isExcludedFromMirroring(win)) {
            qDebug() << win->caption() << "is one of the excluded classes, skipping...";
            return NULL;
        }

        if (onlyCurrentWorkspace && !win->isOnCurrentDesktop()) {
            qDebug() << "Not mirroring window on other workspace:" << win->caption();
            return NULL;
        }
    }

    // TODO: make sure window is contained in current desktop by improving this function
    // fitWindowOnDesktop(win);

    WindowWrapper *vrWin = new WindowWrapper(win);

    bool isChild = isChildWindow(win);

    /* The window that a modal/dialog/menu is created from should have been
     * activated. */
    KWin::EffectWindow *parentWindow = KWin::effects->activeWindow();
    bool isParentMirrored = isChild && !isExcludedFromMirroring(parentWindow)
                            && _kwinWindowToXrdWindow(this, parentWindow) != NULL;
    bool hasParent = parentWindow && isParentMirrored;

    const char *window_title = win->caption().toStdString().c_str();

    if (isChild && !hasParent) {
        qDebug() << "Warning:" << window_title << "should be child but has no parent!";
    }

    const float ppm = 300;
    XrdWindow *xrdWin = xrd_client_window_new_from_pixels(xrdClient,
                                                          window_title,
                                                          win->width(),
                                                          win->height(),
                                                          ppm);
    g_object_set(xrdWin, "native", vrWin, NULL);

    xrd_client_add_window(xrdClient, xrdWin, !(isChild && hasParent), win);

    if (isChild && hasParent) {
        XrdWindow *parentXrdWin = _kwinWindowToXrdWindow(this, parentWindow);
        WindowWrapper *parentVrWin = NULL;
        if (XRD_IS_WINDOW(parentXrdWin)) {
            g_object_get(parentXrdWin, "native", &parentVrWin, NULL);
        }

        /* each window only has one child window, chain this child window to the
         last existing child window of the parent window.
         TODO: does this cover all wayland cases? */
        XrdWindowData *parent_data = xrd_window_get_data(parentXrdWin);
        while (parent_data->child_window != NULL) {
            parentXrdWin = parent_data->child_window->xrd_window;
            g_object_get(parentXrdWin, "native", &parentVrWin, NULL);
            parent_data = xrd_window_get_data(parentXrdWin);
        }

        QRect parentGeometry = parentVrWin->kwinWindow->geometry();
        QPoint parentCenter = parentGeometry.center();
        QPoint childCenter = win->geometry().center();

        QPoint offsetPoint = childCenter - parentCenter;

        graphene_point_t offset = {static_cast<float>(offsetPoint.x()),
                                   -static_cast<float>(offsetPoint.y())};
        xrd_window_add_child(parentXrdWin, xrdWin, &offset);
        qDebug() << "Adding child at" << childCenter << "to parent at" << parentCenter << ", diff "
                 << offsetPoint;
    } else {
        setPositionFromDesktop(xrdWin);
    }

    upload_window(xrdWin);

    // save status so it can be restored when deactivating VR mode
#if KWIN_EFFECT_API_VERSION_MINOR >= 227
    vrWin->keepAboveOrig = win->keepAbove();
    vrWin->keepBelowOrig = win->keepBelow();
#else
    const QVariant keepAboveVariant = win->parent()->property("keepAbove");
    vrWin->keepAboveOrig = keepAboveVariant.toBool();

    const QVariant keepBelowVariant = win->parent()->property("keepBelow");
    vrWin->keepBelowOrig = keepBelowVariant.toBool();
#endif

    putBelow(vrWin->kwinWindow);

    qDebug() << "Created window" << win->caption() << "KeepAbove:" << vrWin->keepAboveOrig
             << "KeepBelow:" << vrWin->keepBelowOrig << win->windowClass() << win->windowRole();

    num_windows++;

    return xrdWin;
}

void segfaultSigaction(int signal, siginfo_t *si, void *arg)
{
    (void) arg;
    printf("Caught segfault at address %p\n", si->si_addr);

    // reset signal handling strategy
    std::signal(signal, SIG_DFL);

    // try to not leave the user with a messed up window configuration before
    // exiting. hope this memory is still intact so we don't segfault there...
    if (instance->m_vrmirrorRunning) {
        instance->restoreAboveBelowStatus();
    }

    // if this didn't crash, re-segfault to create a coredump
    raise(SIGSEGV);
}

void VRMirror::deactivateVRMirror()
{
    qDebug() << "deactivating VR mirror...";

    _disconnectClientSources(this);

    pollTimer->stop();
    delete pollTimer;
    pollTimer = 0;

    if (cursorTexture) {
        g_object_unref(cursorTexture);
        cursorTexture = NULL;
    }
    m_vrmirrorRunning = false;
    num_windows = 0;

    restoreAboveBelowStatus();

    if (XRD_IS_SCENE_CLIENT(xrdClient) && sceneRenderSource != 0) {
        g_source_remove(sceneRenderSource);
        sceneRenderSource = 0;
    }

    g_object_unref(synth);
    synth = NULL;

    GSList *windows = xrd_client_get_windows(xrdClient);
    for (GSList *l = windows; l; l = l->next) {
        XrdWindow *xrdWin = XRD_WINDOW(l->data);
        xrd_window_close(xrdWin);
        /* client unref will do it anyway
        xrd_client_remove_window(xrdClient, xrdWin); */
    }

    g_object_unref(xrdClient);
    xrdClient = NULL;

    std::signal(SIGSEGV, SIG_DFL);
}

void VRMirror::activateVRMirror(bool scene)
{
    if (!KWin::effects->isOpenGLCompositing()) {
        qDebug() << "VR mirror only supported with OpenGL compositing!";
        return;
    }

    sceneRenderSource = 0;
    if (scene) {
        xrdClient = XRD_CLIENT(xrd_scene_client_new());
        xrd_scene_client_initialize(XRD_SCENE_CLIENT(xrdClient));
        // TODO: scene renderer can not run on a timer as long as it is not
        // multi threaded,
        //  because it will block the main loop, synchronizing to the HMD
        //  refresh rate.
        // sceneRenderSource = g_timeout_add(50, _iterate_scene_cb, this);
    } else {
        xrdClient = XRD_CLIENT(xrd_overlay_client_new());
    }
    if (!xrdClient) {
        qDebug() << "Failed to initialize xrdesktop!";
        qDebug() << "Usually this is caused by a problem with the VR runtime.";
        return;
    }

    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfaultSigaction;
    sa.sa_flags = SA_SIGINFO;
    sigaction(SIGSEGV, &sa, NULL);

    QString synth_method = QProcessEnvironment::systemEnvironment().value("INPUTSYNTH", "XDO");
    if (synth_method == "XI2") {
        qDebug() << "Using Synth Method: XI2";
        synth = INPUT_SYNTH(input_synth_new(InputsynthBackend_XI2));
    } else {
        qDebug() << "Using Synth Method: XDO";
        synth = INPUT_SYNTH(input_synth_new(InputsynthBackend_XDO));
    }

    if (!synth) {
        qDebug() << "Failed to initialize input synth";
        return;
    }


    slotUpdateCursorTexture();

    _connectClientSources(this);

    KWin::EffectWindowList order = KWin::effects->stackingOrder();
    for (KWin::EffectWindow *win : order) {
        qDebug() << "Mapping window " << win->caption();
        XrdWindow *xrdWin = mapWindow(win, false);
    }

    qDebug() << "activating VR mirror...";
    pollTimer = new QTimer(this);
    pollTimer->setTimerType(Qt::PreciseTimer);
    pollTimer->start(1);
    connect(pollTimer, SIGNAL(timeout()), this, SLOT(glibIterate()));
    m_vrmirrorRunning = true;
    qDebug() << "VR Mirror activated";

    lastPostPaint = QTime::currentTime();
}

void VRMirror::toggleScreenVRMirror()
{
    if (m_vrmirrorRunning) {
        deactivateVRMirror();
    } else {
        bool scene = sceneModeEnabled;
        activateVRMirror(scene);
    }
}

bool VRMirror::isActive() const
{
    return m_vrmirrorRunning;
}

void VRMirror::setActive(bool active)
{
    qDebug() << "dbus property change:" << active;
    // test if we even need to do something
    if (m_vrmirrorRunning == active) {
        return;
    }

    toggleScreenVRMirror();

    // only emit change signal if toggle did actually work
    if (m_vrmirrorRunning == active) {
        return;
    }
    emit activeChanged(m_vrmirrorRunning);
}

#include "VRMirror.moc"
