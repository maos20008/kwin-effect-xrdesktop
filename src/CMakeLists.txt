#######################################
# Effect

#######################################
# Config

add_subdirectory( kcm )

include(GlibCompileResourcesSupport)
set(RESOURCE_LIST
    res/vr-input-bindings/vrmirror-actions.json
    res/vr-input-bindings/vrmirror-bindings-knuckles-controller.json
    res/vr-input-bindings/vrmirror-bindings-vive-controller.json)
compile_gresources(RESOURCE_FILE
                   XML_OUT
                   TYPE EMBED_C
                   RESOURCES ${RESOURCE_LIST}
                   TARGET res.c)
add_custom_target(resource ALL DEPENDS res.c)

find_package (KWinEffects REQUIRED)


find_package (Qt5 REQUIRED COMPONENTS
    Core
)

find_package (KF5 REQUIRED COMPONENTS
    Service
    WindowSystem
    GlobalAccel
    I18n
)

set(kwin_effect_vrmirror_SRCS VRMirror.cpp kwingltexture.cpp)
qt5_add_resources(kwin_effect_vrmirror_SRCS)
add_library(kwin_effect_xrdesktop SHARED ${kwin_effect_vrmirror_SRCS} res.c)

target_link_libraries(kwin_effect_xrdesktop
PUBLIC
    Qt5::Core
    KF5::WindowSystem
    KF5::Service
    KF5::GlobalAccel
    KF5::I18n
    KWinEffects::KWinEffects
)

target_include_directories(kwin_effect_xrdesktop PRIVATE
    ${KWINXRDEPS_INCLUDE_DIRS}
    ${KWINXRINPUTDEPS_INCLUDE_DIRS}
)
target_link_libraries(kwin_effect_xrdesktop PUBLIC
    ${KWINXRDEPS_LIBRARIES}
    ${KWINXRINPUTDEPS_LIBRARIES}
)

kcoreaddons_desktop_to_json(kwin_effect_xrdesktop vrmirror.desktop SERVICE_TYPES kwineffect.desktop)

install(
    TARGETS
        kwin_effect_xrdesktop
    DESTINATION
        ${PLUGIN_INSTALL_DIR}/kwin/effects/plugins/
)
